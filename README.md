# Shell commands #

* breach-browser
* saloon

# Environment configuration #

* COCKPIT_FUNC_SET : List of Cockpit functionnalities to load.
                     (default : "x11")

# Human-Machine interface #

* [X11](https://bitbucket.org/it2use/cockpit/src/HEAD/hmi/x11)
* [Web](https://bitbucket.org/it2use/cockpit/src/HEAD/hmi/web)
* [Couche](https://bitbucket.org/it2use/cockpit/src/HEAD/hmi/couche)

* [Office](https://bitbucket.org/it2use/cockpit/src/HEAD/hmi/office)
* [Developer](https://bitbucket.org/it2use/cockpit/src/HEAD/hmi/developer)


* [Computer Artist](https://bitbucket.org/it2use/cockpit/src/HEAD/hmi/artist)
* [Computer Designer](https://bitbucket.org/it2use/cockpit/src/HEAD/hmi/cao)

### Packages ###

Live performances :
===================

* htop
* iotop
* kerneltop
* virttop

Shell terminals :
=================

* lxterminal
* tild
* terminator
