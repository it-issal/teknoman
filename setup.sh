#!/bin/sh

for $key in $COCKPIT_FUNC_SET ; do
	TARGET_DIR=$COCKPIT_FUNC_DIR/$key

	if [[ -e $TARGET_DIR ]] ; then
		for folder in src var ; do
		    if [[ ! -d $TARGET_DIR/$folder ]] ; then
			mkdir -p $TARGET_DIR/$folder
		    fi
		done

		if [[ -f $TARGET_DIR/env.sh ]] ; then
			source $TARGET_DIR/env.sh
		fi

		if [[ -f $TARGET_DIR/setup.sh ]] ; then
			. $TARGET_DIR/setup.sh
		fi
	fi
done
