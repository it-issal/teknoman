#!/bin/sh

export APT_PKGs="$APT_PKGs smstools ssmtp"
export APT_PKGs="$APT_PKGs xchat-otr xchat-indicator xchat-gnome-indicator"
export APT_PKGs="$APT_PKGs pidgin" # skype

export PALAVER_PATH=$TARGET_DIR"/opt/Palaver"

alias palaver-sdk='$PALAVER_PATH/sdk'
alias breach-browser='exo-browser --no-sandbox'

export NODEJS_PKGs="$APT_PKGs normit"

alias palaver-='$PALAVER_PATH/sdk '
