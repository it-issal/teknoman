#!/bin/sh

export APT_PKGs="$APT_PKGs sox libsox-fmt-mp3"
export APT_PKGs="$APT_PKGs espeak"

export APT_PKGs="$APT_PKGs mpg123 sox python-argparse libsox-fmt-mp3 mutt wget espeak xvkbd xautomation"

export NODEJS_PKGs="$APT_PKGs normit neutral"

alias speech-translate='normit'
alias speech-transpeak='normit -t'

export PALAVER_PATH=$TARGET_DIR"/opt/Palaver"

palaver_sdk () {
    cd $PALAVER_PATH

    ./sdk $*
}

palaver_plugins () {
    cd $PALAVER_PATH

    ./plugins -i $HOME/Plugins_SDK/$1/$1.sp
}

alias palaver-plugin-create='palaver_sdk -c '
alias palaver-plugin-package='palaver_sdk -p '
alias palaver-plugin-load='palaver_plugins'
