#!/bin/sh

if [[ ! -e "$BREACH_PATH" ]] ; then
    mkdir -p $BREACH_PATH
fi

if [[ ! -e "$BREACH_PATH/exo" ]] ; then
    cd $BREACH_PATH

    BASE_URL="https://raw.githubusercontent.com/breach/releases/master"

    wget -c $BASE_URL/exo_browser/$BREACH_VERSION/exo_browser-$BREACH_VERSION-linux-ia32.tar.gz

    tar zxf exo_browser-$BREACH_VERSION-linux-ia32.tar.gz

    rm -f exo_browser-$BREACH_VERSION-linux-ia32.tar.gz

    mv exo_browser-$BREACH_VERSION-linux-ia32 $BREACH_PATH/exo

    mv $BREACH_PATH/exo/{shell,old-shell}
fi

if [[ ! -e "$BREACH_PATH/exo/shell" ]] ; then
    git clone https://github.com/breach/breach_core.git $BREACH_PATH/exo/shell
fi

cd $BREACH_PATH/exo/shell

npm install
