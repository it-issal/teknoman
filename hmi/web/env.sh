#!/bin/sh

export APT_PKGs="$APT_PKGs chromium-browser firefox thunderbird"

export BREACH_VERSION=$TARGET_DIR"/opt/breach"
export BREACH_PATH=$TARGET_DIR"/opt/breach"

alias exo-browser='$BREACH_PATH/exo/exo_browser'
alias breach-browser='exo-browser --no-sandbox'
