# Features #

* X11 basic & popular apps.
* xRDP : RDP server with builtin Xsession mechanism.

* 3D explorator of filesystems.

### Desktops ###

* i3
* OpenBox
* Cairo-Dock
* Gnome Desktop
* KDE
* LXDE
* Xfce
