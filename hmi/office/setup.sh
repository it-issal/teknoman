#!/bin/sh

docker run -d -e POSTGRES_USER=odoo -e POSTGRES_PASSWORD=odoo --name odoo-db postgres

docker run -v $TARGET_DIR/config/odoo:/etc/odoo -p 127.0.0.1:8069:8069 --name odoo-srv --link db:odoo-db -t odoo

docker stop odoo-srv
docker start -a odoo-srv
