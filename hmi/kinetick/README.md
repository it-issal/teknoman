# Shell commands #

* kinetick

# Features #

Implementation / Maintaining / Support of most Linux projects :

* LCDproc
* LIRC

* [PiFM](https://bitbucket.org/hack2use/jitsu-kinetick/src/HEAD/opt/PiFM/)

### Python APIs ###

* For writing sensors with sonde capabilities & time-related / cumulative triggering.
* For writing scenarios with behavioral & evenemential triggers.
* Powering a Systemd daemon with reflective API.
