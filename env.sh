#!/bin/sh

export APT_PKGs="$APT_PKGs screen xsel x11-apps"
export APT_PKGs="$APT_PKGs leafpad gedit"
export APT_PKGs="$APT_PKGs lxterminal terminator clusterssh"

#*******************************************************************************

if ! which pbcopy &>/dev/null && which xsel &>/dev/null; then
    alias pbcopy='xsel --clipboard --input'
    alias pbpaste='xsel --clipboard --output'
fi

#*******************************************************************************

export COCKPIT_FUNC_DIR=$RONIN_CHARMS_DIR/cockpit/hmi

if [[ $COCKPIT_FUNC_SET == "" ]] ; then
	export COCKPIT_FUNC_SET="web"
fi

for key in `ls $COCKPIT_FUNC_DIR` ; do
	export COCKPIT_FUNC_ALL="$COCKPIT_FUNC_ALL $key"
done

for key in $COCKPIT_FUNC_SET ; do
	TARGET_DIR=$COCKPIT_FUNC_DIR/$key

	if [[ -e $TARGET_DIR ]] ; then
		for folder in opt var ; do
		if [[ -f $TARGET_DIR/env.sh ]] ; then
			source $TARGET_DIR/env.sh
		fi
	fi
done
